## [1.0.2] - 2019-07-15
### Changed
- Heart wiki url from the README now properly redirect to the repository used before _Heart_ version 3

## [1.0.1] - 2019-06-26
### Fixed
- README

## [1.0.0] - 2019-06-21
### Added
- First release (Yay!)
